#!/usr/bin/env node

const program = require('commander');
const fs = require('fs');
const chalk = require('chalk');
const dir = process.cwd();
program.arguments('<file>').option('-m, --model', 'Creates a model').option('-r, --route', 'Creates a route').option('-u, --route_admin', 'Creates a route in /admin').option('-a, --api', 'Creates an API route').option('-v, --view', 'Creates pug file').option('-p, --view_admin', 'Creates pug admin file').option('-f, --full_admin', 'Creates a full admin files').option('-t, --partial', 'Creates a partial file').option('-c, --controller', 'Creates a controller').action((file) => {
	if (program.model) {
		createModel(file);
	}
	if (program.route) {
		createRoute(file);
	}
	if (program.route_admin) {
		createRouteAdmin(file);
	}
	if (program.api) {
		createAPI(file);
	}
	if (program.view) {
		createView(file);
	}
	if (program.view_admin) {
		createAdminView(file);
	}
	if (program.full_admin) {
		createModel(file);
		createRouteAdmin(file);
		// createPartial(file);
		// createAdminView(file);
	}
	if (program.partial) {
		createPartial(file);
	}
	if (program.controller) {
		createController(file);
	}
}).parse(process.argv);

function createFile(from, to, name) {
	fs.readFile(from, 'utf8', (err, data) => {
		if (err) throw err;
		var result = data.replace(/#{name}/g, name);
		result = result.replace(/#{nameUpper}/g, upperFirst(name));
		fs.writeFile(to, result, 'utf8', (err) => {
			if (err) console.log(err);
		});
	});
}

function createModel(name) {
	const finalPath = dir + '/models/' + name + '.model.js';
	log(finalPath);
	createFile(__dirname + '/model', finalPath, name);
}

function createRoute(name) {
	const finalPath = dir + '/routes/' + name + '.route.js';
	log(finalPath);
	createFile(__dirname + '/route', finalPath, name);
}

function createAPI(name) {
	const finalPath = dir + '/api/' + name + '.api.js';
	log(finalPath);
	createFile(__dirname + '/api', finalPath, name);
}

function createRouteAdmin(name) {
	const finalPath = dir + '/routes/admin/' + name + '.route.js';
	log(finalPath);
	createFile(__dirname + '/route_admin', finalPath, name);
}

function createView(name) {
	const pathPug = dir + '/views/' + name + '.pug';
	const pathJs = dir + '/public/src/js/' + name + '.js';
	const pathScss = dir + '/public/src/css/' + name + '.scss';
	log(pathPug);
	createFile(__dirname + '/default_view.pug', pathPug, name);
	log(pathJs);
	fs.createReadStream(__dirname + '/default_scss.scss').pipe(fs.createWriteStream(pathScss));
	log(pathScss);
	fs.createReadStream(__dirname + '/default_js.js').pipe(fs.createWriteStream(pathJs));
}

function createAdminView(name) {
	const pathPug = dir + '/views/admin/' + name + '.pug';
	const pathJs = dir + '/public/src/js/admin/' + name + '.js';
	const pathScss = dir + '/public/src/css/admin/' + name + '.scss';
	log(pathPug);
	createFile(__dirname + '/adm_view.pug', pathPug, name);
	log(pathJs);
	fs.createReadStream(__dirname + '/adm_scss.scss').pipe(fs.createWriteStream(pathScss));
	log(pathScss);
	fs.createReadStream(__dirname + '/adm_js.js').pipe(fs.createWriteStream(pathJs));
}

function createPartial(name) {
	const finalPath = dir + '/views/admin/partials/' + name + '.pug';
	log(finalPath);
	createFile(__dirname + '/partial.pug', finalPath, name);
}

function createController(name) {
	const finalPath = dir + '/public/src/js/admin/ctrl/' + name + '.controller.js';
	log(finalPath);
	createFile(__dirname + '/controller', finalPath, name);
}

function log(text) {
	console.log('Creating ' + chalk.underline(text));
}

function upperFirst(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}